# merge-request-to-csv

The purpose of this script is to generate a CSV file that contains all of the merge request in all of the project under an organization

## Prequisities

- NodeJS v18.11.0.
- yarn version v1.22.19

## Getting started

This project is using gitbeaker https://github.com/jdalrymple/gitbeaker. If you would like to develop it further, please look into the GitBeaker documentation

1. To install the dependencies run the following command.

```
yarn serve
```

2. Afterwards you will need to generate a personal access token using the following instructions
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

3. Add that token to the `script.js` https://gitlab.com/demos/scripts/merge-request-to-csv/-/blob/main/script.js#L5

![Create access token](assets/access-token.png)

4. Run the script by using the command `node script.js`

## 💥 Voila! You should have a merge-request.csv created.
