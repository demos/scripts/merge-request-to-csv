import { Gitlab } from '@gitbeaker/node';
import { appendFileSync } from 'fs';

const api = new Gitlab({
  token: '',
});

const today = new Date()
const fileDate = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDay()}`

// Create merge-request.csv
const columns = `project_url,project_id,merge_request\n`;
try {
  appendFileSync("./merge-requests-" + fileDate + ".csv", columns);
} catch (err) {
  console.error(err);
}

// Get all projects
let projects = await api.Projects.all({ maxPages: 2 });
// Loop through projects
for (const project of projects) {
  console.log(project.id)
// List all MR's from project
  let mr = await api.MergeRequests.all({projectId: project.id})
// Export merge request to CSV
  const row = `${project.web_url},${project.id},${mr}\n`
  try {
    appendFileSync("./merge-requests-" + fileDate + ".csv", row);
    console.log(row)
  } catch (err) {
    console.error("There was an error:" + err);
  }
}





